package battleship;

import java.awt.Point;
import java.util.Vector;

public class Ship
{
	private Point myFrontLocation;
	private Point myBackLocation;
	private int myLength;
	private Vector<Boolean> myHits;
	private boolean mySunk;
	private ShipType myShipType;
	private Vector<Point> myShipPoints = new Vector<Point>();
	
	public Ship(ShipType ship, int length)
	{
		myLength = length;
		myHits = new Vector<Boolean>();
		for(int i = 0; i < myLength; ++i)
		{
			myHits.add(false);
		}
		myFrontLocation = null;
		myBackLocation = null;
		mySunk = false;
		myShipType = ship;
	}
	public boolean placePeg(Point location)
	{
		for(Point p : myShipPoints)
		{
			if(location.equals(p) && myHits.get(myShipPoints.indexOf(p)) == false)
			{
				myHits.set(myShipPoints.indexOf(p), true);
				return true;
			} 
		}
		return false;
	}
	public boolean isSunk()
	{
		int pegCounter = 0;
		for(int i = myLength - 1; i >= 0; --i)
		{
			if(myHits.get(i) == true)
			{
				++pegCounter;
			}
		}
		if(pegCounter == myLength)
		{
			mySunk = true;
		}
		pegCounter = 0;
		return mySunk;
	}
	public boolean setFrontLocation(Point location)
	{
		if(location.getX() >= 0 && location.getX() <= 9 && location.getY() >= 0 && location.getY() <= 9)
		{
			myFrontLocation = location;
			return true;	
		}
		else
		{
			return false;
		}
	}
	public boolean setBackLocation(Point location)
	{
		if(myFrontLocation == null)
		{
			return false;
		}
		if(location.getX() >= 0 && location.getX() <= 9 && location.getY() >= 0 && location.getY() <= 9)
		{
			if((location.getX() != myFrontLocation.getX()) && (location.getY() != myFrontLocation.getY()))
			{
				return false;
			}
			myBackLocation = location;
			setShipPoints();
			return true;	
		}
		else
		{
			return false;
		}
	}
	public void setShipPoints()
	{
		if(myFrontLocation.getX() == myBackLocation.getX())
		{
			int tempX = (int)myFrontLocation.getX();
			int frontY = (int)myFrontLocation.getY();
			int backY = (int)myBackLocation.getY();
			if(frontY > backY)
			{
				for(int i = 0; i < myLength; ++i)
				{
					myShipPoints.add(new Point(tempX, backY));
					++backY;
				}
			}
			else
			{
				for(int i = 0; i < myLength; ++i)
				{
					myShipPoints.add(new Point(tempX, frontY));
					++frontY;
				}
			}
		}
		else
		{
			int tempY = (int)myFrontLocation.getY();
			int frontX = (int)myFrontLocation.getX();
			int backX = (int)myBackLocation.getX();
			if(frontX > backX)
			{
				for(int i = 0; i < myLength; ++i)
				{
					myShipPoints.add(new Point(backX, tempY));
					++backX;
				}
			}
			else
			{
				for(int i = 0; i < myLength; ++i)
				{
					myShipPoints.add(new Point(frontX, tempY));
					++frontX;
				}
			}
		}
	}
	public void resetShipPoints()
	{
		myFrontLocation = null;
		myBackLocation = null;
		myShipPoints.clear();
	}
	public Point getFrontLocation()
	{
		return myFrontLocation;
	}
	public Point getBackLocation()
	{
		return myBackLocation;
	}
	public ShipType getShipType()
	{
		return myShipType;
	}
	public int getLength()
	{
		return myLength;
	}
	public Vector<Boolean> getHits()
	{
		return myHits;
	}
	public Vector<Point> getShipPoints()
	{
		return myShipPoints;
	}
}
