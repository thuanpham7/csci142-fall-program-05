package battleship;

import java.awt.Point;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JOptionPane;

public class GameModel 
{
	private ShipBoard myShipBoard;
	private ShipBoard myComputerBoard;
	private String myPlayerName;
	private HashMap<String, Vector<Point>> myHashMap;
	private HashMap<String, Vector<Point>> computerHashMap;
	private boolean myIsComputerTurn = false;
	private String myMessage;
	private AILevel myAILevel;
	private Vector<Point> myPoint;
	private Vector<Point> compPoint;
	private Point move; 
	
	public GameModel(String name, AILevel level)
	{
		myAILevel = level;
		myPlayerName = name; 
	}
	public void beginGame()
	{
		myComputerBoard = new ShipBoard();
		myComputerBoard.placeShips();
		compPoint = myComputerBoard.getPoints();
		computerHashMap = myComputerBoard.getMap();
	}
		
	public boolean checkIfWinner()
	{
		
		return false;
	}
	public String generateMessage()
	{
		return null;
	}
	public ShipBoard getShipBoard()
	{
		return myShipBoard;
	}
	public ShipBoard getComputerBoard()
	{
		return myComputerBoard;
	}
	public String getPlayerName()
	{
		return myPlayerName;
	}
	
	public void switchturn() {
		if (myIsComputerTurn == false) {
			myIsComputerTurn = true;
		} else {
			myIsComputerTurn = false;
		}
	}
	
	public String getMessage()
	{
		return myMessage;
	}
	public AILevel getAILevel()
	{
		return myAILevel;
	}
	public Vector<Point> getMyPoints(){
		return myPoint;
	}
	public Vector<Point> getCompPoints(){
		return compPoint;
	}
	public Point getMove() {
		return move;
	}
	public boolean getComputerTurn() {
		return myIsComputerTurn;
	}
	public ShipBoard getMyShip() {
		return myShipBoard;
	}
	
	public HashMap<String, Vector<Point>> getcompMap(){
		return computerHashMap;
	}
	

}
