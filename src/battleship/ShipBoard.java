package battleship;

import java.awt.Point;
import java.util.HashMap;
import java.util.Vector;

public class ShipBoard 
{
	private int SIZE = 10;
	
	private PegType[][] myGrid;
	private PegType myLastMove;
	private Vector<Ship> myShips;
	private Vector<Point> myPoints;
	private HashMap<String, Vector<Point>> myhashMap;
	private ShipBoard myShipBoard;
	Vector<Point> destroyerPoint;
	Vector<Point> cruiserPoint;
	Vector<Point> submarinePoint;
	Vector<Point> battleshipPoint;
	Vector<Point> carrierPoint;
	
	public ShipBoard()
	{
		myGrid = new PegType[SIZE][SIZE];
		myLastMove = PegType.NONE;
		for(int i = SIZE - 1; i >= 0; --i)
		{
			for(int j = SIZE - 1; j >= 0; --j)
			{
				myGrid[i][j] = PegType.NONE;
			}
		}
		myShips = new Vector<Ship>();
		myPoints = new Vector<Point>();
		myShips.add(new Ship(ShipType.DESTROYER, 2));
		myShips.add(new Ship(ShipType.CRUISER, 3));
		myShips.add(new Ship(ShipType.SUBMARINE, 3));
		myShips.add(new Ship(ShipType.BATTLESHIP, 4));
		myShips.add(new Ship(ShipType.CARRIER, 5));
		myhashMap = new HashMap<>();
		myhashMap.put(ShipType.DESTROYER.toString(), new Vector<Point>());
		myhashMap.put(ShipType.CRUISER.toString(), new Vector<Point>());
		myhashMap.put(ShipType.SUBMARINE.toString(), new Vector<Point>());
		myhashMap.put(ShipType.BATTLESHIP.toString(), new Vector<Point>());
		myhashMap.put(ShipType.CARRIER.toString(), new Vector<Point>());
	}
	public boolean placePeg(Point point)
	{
		int xPoint = (int)point.getX();
		int yPoint = (int)point.getY();
		if(isValidMove(point) == false)
		{
			return false;
		}
		else
		{
			for(Ship s : myShips)
			{
				if(s.placePeg(point))
				{
					myLastMove = PegType.RED;
					myGrid[xPoint][yPoint] = PegType.RED;
					return true;
				}
			}
			myLastMove = PegType.WHITE;
			myGrid[xPoint][yPoint] = PegType.WHITE;
			return true;
		}
	}
	public boolean isValidMove(Point point)
	{
		int xPoint = (int)point.getX();
		int yPoint = (int)point.getY();
		if(xPoint < 0 || xPoint > 9)
		{
			return false;
		}
		else if(yPoint < 0 || yPoint > 9)
		{
			return false;
		}
		else if(!myGrid[xPoint][yPoint].equals(PegType.NONE))
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
	public boolean placeShips()
	{
		Vector<Point> tempPoints = new Vector<Point>();
		for(int i = 0; i < 10; ++i)
		{
			for(int j = 0; j < 10; ++j)
			{
				tempPoints.add(new Point(i, j));
			}
		}
		for(Ship s : myShips)
		{
			boolean valid = false;
			while(valid == false)
			{
				int random = (int)(Math.random() * tempPoints.size());
				Point tempFront = tempPoints.get(random);
				Vector<Point> backLocPoss = validShipOptions(s, tempFront);
				int random2 = (int)(Math.random() * backLocPoss.size());
				Point tempBack = backLocPoss.get(random2);
				if(placeShip(s.getShipType(), tempFront, tempBack))
				{
					valid = true;
					tempPoints.removeAll(myPoints);
				}
			}
		}
		return true;
	}
	private Vector<Point> validShipOptions(Ship s, Point y)
	{
		int addOrSub = s.getLength() - 1;
		int frontLocX = (int)y.getX();
		int frontLocY = (int)y.getY();
		Vector<Point> possPoints = new Vector<Point>();
		Vector<Point> validPoints = new Vector<Point>();
		int x1 = frontLocX + addOrSub;
		int x2 = frontLocX - addOrSub;
		int y1 = frontLocY + addOrSub;
		int y2 = frontLocY - addOrSub;
		Point x1Point = new Point(x1, frontLocY);
		Point x2Point = new Point(x2, frontLocY);
		Point y1Point = new Point(frontLocX, y1);
		Point y2Point = new Point(frontLocX, y2);
		possPoints.add(x1Point);
		possPoints.add(x2Point);
		possPoints.add(y1Point);
		possPoints.add(y2Point);
		for(Point p : possPoints)
		{
			if(!(p.getX() > 9 || p.getX() < 0 || p.getY() > 9 || p.getY() < 0))
			{
				validPoints.add(p);
			}
		}
		return validPoints;
	}
	public void resetBoard()
	{
		myShips.clear();
		for(int i = SIZE - 1; i >= 0; --i)
		{
			for(int j = SIZE - 1; j >= 0; --j)
			{
				myGrid[i][j] = PegType.NONE;
			}
		}
		myLastMove = PegType.NONE;
		myShips.add(new Ship(ShipType.DESTROYER, 2));
		myShips.add(new Ship(ShipType.CRUISER, 3));
		myShips.add(new Ship(ShipType.SUBMARINE, 3));
		myShips.add(new Ship(ShipType.BATTLESHIP, 4));
		myShips.add(new Ship(ShipType.CARRIER, 5));
	}
	public boolean placeShip(ShipType type, Point front, Point back)
	{
		for(Ship s : myShips)
		{
			if(s.getShipType().equals(type))
			{
				if(!s.setFrontLocation(front)) return false;
				if(!s.setBackLocation(back))
				{
					s.resetShipPoints();
					return false;
				}
				Vector<Point> tempPoints = s.getShipPoints();
				if(myPoints.size() > 0)
				{
					for(Point p : tempPoints)
					{
						for(Point z : myPoints)
						{
							if(z.equals(p))
							{
								s.resetShipPoints();
								return false;
							}
						}
					}
				}
				for(Point p : s.getShipPoints())
				{
					myPoints.add(p);
					if (s.getShipPoints().size() == 2) {
						myhashMap.put(ShipType.DESTROYER.toString(), s.getShipPoints());
					} else if (s.getShipPoints().size() == 3) {
						myhashMap.put(ShipType.CRUISER.toString(), s.getShipPoints());
					} else if (s.getShipPoints().size() == 3) {
						myhashMap.put(ShipType.SUBMARINE.toString(), s.getShipPoints());
					}	else if (s.getShipPoints().size() == 4) {
						myhashMap.put(ShipType.BATTLESHIP.toString(), s.getShipPoints());
					} else {
						myhashMap.put(ShipType.CARRIER.toString(), s.getShipPoints());
					}
				}
				return true;
			}
		}		
		return false;
	}
	public int numberShipsSunk()
	{
		int counter = 0;
		for(Ship s : myShips)
		{
			if(s.isSunk())
			{
				++counter;
			}
		}
		return counter;
	}
	public boolean allShipsSunk()
	{
		if(numberShipsSunk() == 5) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public PegType getLastMove()
	{
		return myLastMove;
	}
	public PegType[][] getGrid()
	{
		return myGrid;
	}
	public Vector<Ship> getShips()
	{
		return myShips;
	}
	public Vector<Point> getPoints()
	{
		return myPoints;
	}
	public HashMap getMap() { 
		return myhashMap;
	}
	
}
