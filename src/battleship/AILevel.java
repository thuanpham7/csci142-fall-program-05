package battleship;

public enum AILevel 
{
	RANDOM_MODE ("Random"),
	CHEAT_MODE ("Cheat"),
	INTELLIGENT_MODE ("Intelligent");
	
	private final String myLevel;
	
	private AILevel(String level) 
	{
		myLevel = level;
	}
	public String getLevel()
	{
		return myLevel;
	}
}
