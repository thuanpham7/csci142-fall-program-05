package battleship;

import java.awt.Point;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

public class DecisionMaker
{
	private ShipBoard myShipBoard;
	private Point myLastMove;
	private HashMap<String, Vector<Point>> myHashMap;
	private AILevel myLevel;
	private Vector<Point> cheatStorage = new Vector<Point>();
	private Point next;
	private Vector<Point> myPoints;
	
	public DecisionMaker(AILevel level, ShipBoard board)
	{
		myShipBoard = board;
		board.placeShips();
		myHashMap = new HashMap<>();
		myHashMap = myShipBoard.getMap();
		myLevel = level;
		for(Point p : myShipBoard.getPoints())
		{
			cheatStorage.add(p);
		}
	}
	public Point nextMove()
	{
		if(myLevel.equals(AILevel.RANDOM_MODE))
		{
			boolean validMove = false;
			int randX;
			int randY;
			int tempX = -1;
			int tempY = -1;
			while(!validMove)
			{
				randX = (int)(Math.random() * 10);
				randY = (int)(Math.random() * 10);
				validMove = myShipBoard.isValidMove(new Point(randX, randY));
				if(validMove)
				{
					tempX = randX;
					tempY = randY;
				}
			}
			next = new Point(tempX, tempY);
			return next;
		}
		else if(myLevel.equals(AILevel.CHEAT_MODE))
		{
			Point next = cheatStorage.get(0);
			cheatStorage.remove(0);
			return next;
		}
		else
		{
			return intelligentNextMove();
		}
	}
	public Point intelligentNextMove()
	{
		Vector<Point> tempPoints = new Vector<Point>();
		for(Vector<Point> v : myHashMap.values())
		{
			int xPoint = (int)v.get(0).getX();
			int yPoint = (int)v.get(0).getY();
			if(v.size() == 1)
			{
				int pot1 = xPoint + 1;
				int pot2 = xPoint -1;
				int pot3 = yPoint + 1;
				int pot4 = yPoint - 1;
				tempPoints.add(new Point(pot1, yPoint));
				tempPoints.add(new Point(pot2, yPoint));
				tempPoints.add(new Point(xPoint, pot3));
				tempPoints.add(new Point(xPoint, pot4));
				for(Point i : tempPoints)
				{
					if(myShipBoard.isValidMove(i))
					{
						return i;
					}
				}
			}
			else if(v.size() > 1)
			{
				Point pointA = v.get(0);
				Point pointB = v.get(1);
				boolean verticalShip = false;
				if(pointA.getX() == pointB.getX())
				{
					verticalShip = true;
				}
				if(verticalShip)
				{
					int xConstant = (int)pointA.getX();
					Vector<Integer> yKnown = new Vector<Integer>();
					for(Point d : v)
					{
						int tempY = (int)d.getY();
						yKnown.add(tempY);
						Collections.sort(yKnown);
					}
					for(int m : yKnown)
					{
						tempPoints.add(new Point(xConstant, m - 1));
						tempPoints.add(new Point(xConstant, m + 1));
					}
				}
				else
				{
					int yConstant = (int)pointA.getY();
					Vector<Integer> xKnown = new Vector<Integer>();
					for(Point d : v)
					{
						int tempX = (int)d.getX();
						xKnown.add(tempX);
						Collections.sort(xKnown);
					}
					for(int m : xKnown)
					{
						tempPoints.add(new Point(m - 1, yConstant));
						tempPoints.add(new Point(m + 1, yConstant));
					}
				}
				for(Point i : tempPoints)
				{
					if(myShipBoard.isValidMove(i))
					{
						boolean returnPoss = true;
						for(Point t : v)
						{
							if(t.equals(i))
							{
								returnPoss = false;
								break;
							}
						}
						if(returnPoss)
						{
							return i;
						}	
					}
				}
			}
			else
			{
				
			}
		}
		boolean validMove = false;
		Point next;
		int tempX = -1;
		int tempY = -1;
		while(!validMove)
		{
			int randX = (int)(Math.random() * 10);
			int randY = (int)(Math.random() * 10);
			validMove = myShipBoard.isValidMove(new Point(randX, randY));
			if(validMove)
			{
				tempX = randX;
				tempY = randY;
			}
		}
		next = new Point(tempX, tempY);
		return next;
	}
	public void placeNextMove()
	{
		myLastMove = nextMove();
		myShipBoard.placePeg(myLastMove);
		for(Ship s : myShipBoard.getShips())
		{
			for(Point p : s.getShipPoints())
			{
				if(myLastMove.equals(p))
				{
					mapModifier(s, p);
					break;
				}
			}
		}
		mapRemover();
	}
	private void mapModifier(Ship s, Point p)
	{
		String tempType = s.getShipType().toString();
		myHashMap.get(tempType).add(p);
	}
	private void mapRemover()
	{
		Vector<String> shipStrings = new Vector<String>();
		for(Ship s : myShipBoard.getShips())
		{
			if(s.isSunk())
			{
				shipStrings.add(s.toString());
			}
		}
		for(String s : shipStrings)
		{
			myHashMap.remove(s);
		}
	}
	public AILevel getLevel()
	{
		return myLevel;
	}
	public void setLastMove(Point point)
	{
		if(myShipBoard.isValidMove(point))
		{
			myShipBoard.placePeg(point);
		}
		myLastMove = point;
	}
	public Point getLastMove()
	{ 
		return myLastMove;
	}
	public ShipBoard getShipBoard()
	{
		return myShipBoard;
	}
	public void setHashMap(HashMap<String, Vector<Point>> map)
	{
		myHashMap = map;
	}
	public HashMap<String, Vector<Point>> getHashMap()
	{
		return myHashMap;
	}
	
	public Point getMove() {
		return next;
	}
	public Vector<Point> getPoints()
	{
		myPoints = myShipBoard.getPoints();
		return myPoints;
	}
	
}