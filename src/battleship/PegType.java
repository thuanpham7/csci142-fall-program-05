package battleship;

public enum PegType
{
	RED ("Red"),
	WHITE ("White"),
	NONE ("None");
	
	private final String myPeg;
	
	private PegType(String peg)
	{
		myPeg = peg;
	}
	public String getType() 
	{
		return myPeg;
	}
}
