package battleship;

public enum ShipType 
{
	CRUISER ("Cruiser"),
	CARRIER ("Carrier"),
	BATTLESHIP ("Battleship"),
	SUBMARINE ("Submarine"),
	DESTROYER ("Destroyer");
	
	private final String myShip;
	
	private ShipType(String ship)
	{
		myShip = ship;
	}
	public String getType()
	{
		return myShip;
	}
	public int getLength() 
	{
		if(myShip=="Destroyer")
		{
			return 2;
		}
		if(myShip=="Cruiser")
		{
			return 3;
		}
		if(myShip=="Submarine")
		{
			return 3;
		}
		if(myShip=="Battleship")
		{
			return 4;
		}
		if(myShip=="Carrier")
		{
			return 5;
		}
		return 0;
	}
}
