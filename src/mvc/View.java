package mvc;
/**
 * Simple view class used to show how Model-View-Controller
 * is implemented.  This is also the main application.
 * Version 2.0 uses Generics.
 *
 * @author Daniel Plante
 * @version 1.0 (28 January 2002)
 * @version 2.0 (1 February 2008)
 */

import java.awt.*;
import java.lang.reflect.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class View 
{
    //////////////////////
    //    Properties    //
    //////////////////////
    
    public final static int myNumSquares = 10;
    private Can[] mySquare;
    private JPanel mySquaresPanel;
    private ButtonListener[] mySquareListener;
    private JTextField myTextField;
    private Controller myController;
    private Image myXImage;
    //private Image myOImage;
    private Image myBlankImage;
    
    ///////////////////////
    //      Methods      //
    ///////////////////////
    
    /**
     * View constructor used to lay out the view
     *
     * <pre>
     * pre:  none
     * post: the view is set up and initialized
     * </pre>
     */
    public View(Controller controller)
    {
        String value;
        int i;
                		
        JFrame frame = new JFrame("MVC Basics with Parameters");
        
        frame.setSize(600,600);
        frame.setLayout(null);
        frame.setBackground(Color.gray);

        myXImage = Toolkit.getDefaultToolkit().getImage("images/pX.jpg");
        myBlankImage = Toolkit.getDefaultToolkit().getImage("images/blank.jpg");
        
        mySquare = new Can[myNumSquares];
        
        mySquareListener = new ButtonListener[myNumSquares];
        mySquaresPanel = new JPanel(new GridLayout(myNumSquares,1));
        mySquaresPanel.setSize(150, 240);
        mySquaresPanel.setLocation(100,80);
        
        for(i=0; i < 10; i++)
        {
           mySquare[i] = new Can(myBlankImage);
           mySquaresPanel.add(mySquare[i]);
        } 

        
        myController = controller;
             
        value = myController.getModelValue();
        myTextField = new JTextField(value);
        myTextField.setSize(100, 50);
        myTextField.setLocation(400, 400);

        frame.add(mySquaresPanel);
        frame.add(myTextField);
        
        this.associateListeners();
        
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Associates each component's listener with the controller
     * and the correct method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    public void associateListeners()
    {
        Class<? extends Controller> controllerClass;
        Method incrementMethod;
        Class<?>[] classArgs;

        controllerClass = myController.getClass();
        
        incrementMethod = null;
        classArgs = new Class[1];
        
        try
        {
           classArgs[0] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException e)
        {
           String error;
           error = e.toString();
           System.out.println(error);
        }
        
        
        try
        {
           incrementMethod = controllerClass.getMethod("increment",classArgs);      
        }
        catch(NoSuchMethodException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        
        int i;
        Integer[] args;

        for (i=0; i < myNumSquares; i++)
        {
           args = new Integer[1];
           args[0] = new Integer(i);
           mySquareListener[i] = 
                   new ButtonListener(myController, incrementMethod, args);
           
           mySquare[i].addMouseListener(mySquareListener[i]);

        }
    }
    
    /*
     * Purposely have some weird display logic here, just taking
     * mod 2 of the num.  You would want to have "real" logic 
     * for changing the image, like a real game with a purposeful
     * choice of X, O or no image.
     */
    public void changeImage(int num, int row)
    {
        if(0 == num % 2)
        {
          mySquare[row].setImage(myXImage);
        }
        else
        {
          mySquare[row].setImage(myBlankImage);
        }
    }

    /**
     * Updates myTextField with the String text.
     *
     * @param text the text string to use in updating the text field
     */
    public void setTextField(String text)
    {
        myTextField.setText(text);
    }
    
}