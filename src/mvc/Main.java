package mvc;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.*;

import battleship.AILevel;
import battleship.GameModel;
import battleship.ShipBoard;
import battleship.DecisionMaker;


public class Main extends JFrame implements ActionListener {
	private static GameModel model;
	private static AILevel myMode;
	private static DecisionMaker game;
	private static Vector<Point> myPoint;
	private static Vector<Point> compPoint;
	private static BeginGamePopUp pop;
	private static GetShipName nameShip;
	private static HashMap<String, Vector<Point>> compHashMap;
	private static HashMap<String, Vector<Point>> myHashMap;
	private int place = 0;
	private int place_1 = 0;
	ImageIcon water = new ImageIcon("realwater.jpg");
	ImageIcon fire = new ImageIcon("giphy.gif");
	ImageIcon target = new ImageIcon("target.gif");
	ImageIcon bslogo = new ImageIcon("logo.jpg");
	ImageIcon dplante = new ImageIcon("dplanteresize.jpg");
	JButton[][] ship = new JButton[10][10];
	JButton[][] peg = new JButton[10][10];
	JButton player;
	JButton reset;
	JButton computer;
	private static String ShipName;
	public static void main(String[] args) {

		//Pop up the main picture of the game
		nameShip = new GetShipName();
		pop = new BeginGamePopUp();
		pop.popUpMessage();

		String name = JOptionPane.showInputDialog("Enter player name");				//Ask the user the name
		String[] options = {"CHEAT_MODE", "INTELLIGENT_MODE", "RANDOM_MODE"};

		//Show all the options
		int response = JOptionPane.showOptionDialog(null, "Select mode", "Welcome to BattleShipGame", 0, 1, null, options, options[0]);
		switch(response) {
		case 0: 														//Cheat mode case will activate when click cheat mode
			myHashMap = new HashMap<String, Vector<Point>>();
			compHashMap = new HashMap<String, Vector<Point>>();
			model = new GameModel(name,AILevel.CHEAT_MODE);
			myMode = AILevel.CHEAT_MODE;
			model.beginGame();
			game = new DecisionMaker(myMode, new ShipBoard());
			myPoint = game.getPoints();
			compPoint = model.getCompPoints();
			compHashMap = model.getcompMap();
			myHashMap = game.getHashMap();
			new Main(name, null);  
			break;
		case 1: 														//intelligent mode case will activate when click cheat mode
			myHashMap = new HashMap<String, Vector<Point>>();
			compHashMap = new HashMap<String, Vector<Point>>();
			model = new GameModel(name,AILevel.INTELLIGENT_MODE);
			myMode = AILevel.INTELLIGENT_MODE;
			model.beginGame();
			compPoint = model.getCompPoints();
			game = new DecisionMaker(myMode, new ShipBoard());
			myPoint = game.getPoints();
			compHashMap = model.getcompMap();
			myHashMap = game.getHashMap();
			new Main(name, null);  
			break;
		case 2: 														////random mode case will activate when click cheat mode
			myHashMap = new HashMap<String, Vector<Point>>();
			compHashMap = new HashMap<String, Vector<Point>>();
			model = new GameModel(name,AILevel.RANDOM_MODE);
			myMode = AILevel.RANDOM_MODE;
			model.beginGame();
			game = new DecisionMaker(myMode, new ShipBoard());
			myPoint = game.getPoints();
			compPoint = model.getCompPoints();
			compHashMap = model.getcompMap();
			myHashMap = game.getHashMap();
			new Main(name, null); 
			break;
		}
	}

	public Main(String name, ActionEvent e) {
		JFrame frame = new JFrame("BattleShip");					//Set up frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel ShipPanel = new JPanel();
		JPanel PegPanel = new JPanel();

		//Set up Ship Panel's button for placing ComputerPeg (Left-side)
		JPanel firstPanel = new JPanel();
		firstPanel.setLayout(new GridLayout(10, 10));
		for (int i=0; i< ship.length; i++) {
			for (int j=0; j<ship[i].length; j++) {
				ship[i][j] = new JButton();
				ship[i][j].setPreferredSize(new Dimension(100, 100));
				ship[i][j].addActionListener(this);
				firstPanel.add(ship[i][j]);
			}
		}

		//Set up PegPanel's Buttons for placing PlayerPeg (Right-side)
		JPanel secondPanel = new JPanel();
		secondPanel.setLayout(new GridLayout(10, 10));
		for (int i=0; i<peg.length; i++) {
			for (int j=0; j<peg[i].length; j++) {
				peg[i][j] = new JButton();;
				peg[i][j].setPreferredSize(new Dimension(100, 100));
				peg[i][j].addActionListener(this);
				secondPanel.add(peg[i][j]);
			}
		}

		//Set up the player and the computer turn and the reset button
		ShipPanel.add(firstPanel);
		PegPanel.add(secondPanel);
		JPanel turn = new JPanel();
		turn.setLayout(new GridLayout(1,3));
		player = new JButton(name + " Turn");
		reset = new JButton("Reset the game");
		computer = new JButton("Computer Turn");
		player.setPreferredSize(new Dimension(50,150));
		reset.setPreferredSize(new Dimension(50,50));
		computer.setPreferredSize(new Dimension(50,150));
		turn.add(player);
		turn.add(computer);
		turn.add(reset);
		player.addActionListener(this);
		computer.addActionListener(this);
		reset.addActionListener(this);
		frame.add(ShipPanel, BorderLayout.WEST);
		frame.add(PegPanel, BorderLayout.EAST);
		frame.add(turn, BorderLayout.SOUTH);
		frame.setSize(520,600);
		frame.setMinimumSize(new Dimension(3000,3000));
		frame.setVisible(true);

		//Set up the Grid Background Color
		for (int i = 0; i< 10; i++) {
			for (int j = 0; j<10;j++) {
				peg[i][j].setIcon(water);
			}
		}
		for (int i = 0; i< 10; i++) {
			for (int j = 0; j<10;j++) {
				ship[i][j].setIcon(water);
			}
		}
	}
	// Set up GamePlay
	public void actionPerformed(ActionEvent e) {
		if (model.getComputerTurn() == false) {
			PlayerTurn(e);
		}else {											
			if (e.getSource() == computer){					//Click on the Computer Turn when it's blue to activate the computer's move
				player.setBackground(Color.BLUE);
				ComputerTurn(e);
			}
		}


		//Reset Button
		if (e.getSource() == reset) {
			for (int i = 0; i< 10; i++) {
				for (int j = 0; j<10;j++) {
					peg[i][j].setIcon(water);
					ship[i][j].setIcon(water);
				}
			}
			model.beginGame(); //Restarting the game when hit reset button
			game = new DecisionMaker(myMode, new ShipBoard());
			myPoint = game.getPoints();
			compPoint = model.getCompPoints();
		}
		//	


	}

	//Set up Player Turn with Overlapping pegs error message
	public void PlayerTurn(ActionEvent e) {
		boolean valid = true;
		player.setBackground(null);
		if (model.getComputerTurn() == false) {
			for (int i = 0; i< 10; i++) {
				for (int j = 0; j<10;j++) {
					if (e.getSource()==peg[i][j]) {
						//Check if there is already a hit there
						if ((peg[i][j].getIcon() == fire)){
							player.setBackground(Color.BLUE);
							computer.setBackground(null);
							JOptionPane.showMessageDialog(null, "Can't choose the peg twice! Choose again brothaaa!!!");
							model.switchturn(); //Back to the player turn when overlapping peg happens
							valid = false;
						}
					}
					//Placing fire as hit
					for (int k = 0; k <compPoint.size(); k++) {
						if ((compPoint.get(k).x == j) && (compPoint.get(k).y == i)){
							if (e.getSource()==peg[i][j]) {
								peg[i][j].setIcon(fire);
								//Show what specific ship is hit
								nameShip.getKey(compHashMap, compPoint.get(k));
								ShipName = nameShip.getShipName();
								JOptionPane.showMessageDialog(null, "Hit " + ShipName);
								place += 1;
								if (valid == false) {
									computer.setBackground(null);
								} else {
									computer.setBackground(Color.BLUE);
									valid = true;
								}
							} 
						}
					}
					//Placing miss 
					if (e.getSource()==peg[i][j]) {
						if (peg[i][j].getIcon() == water) {
							peg[i][j].setIcon(dplante);
							computer.setBackground(Color.BLUE);
							//Check if there is already a peg there
						}else if (e.getSource()==peg[i][j]) {
							if ((peg[i][j].getIcon() == dplante)){
								JOptionPane.showMessageDialog(null, "Can't choose the peg twice! Choose again brothaaa!!!");
								model.switchturn();
								player.setBackground(Color.BLUE);
								computer.setBackground(null);
							}
						}
					} 
				}
			}
		}
		if (place == 17) {
			JOptionPane.showMessageDialog(null, "You wins");
		}
		model.switchturn(); // Switch turn when the player has appropriate move
	}

	//Set up Computer Turn with no overlapping pegs
	public void ComputerTurn(ActionEvent e) {
		game.placeNextMove(); //Auto next move of the computer
		//Check if there is any peg already in that position
		if ((ship[game.getLastMove().y][game.getLastMove().x].getIcon() == dplante) || (ship[game.getLastMove().y][game.getLastMove().x].getIcon() == fire)){
			game.placeNextMove();
		}
		//Place peg when hit
		for (int i = 0; i < myPoint.size(); i++) {
			if ( (game.getLastMove().x == myPoint.get(i).x) && (game.getLastMove().y == myPoint.get(i).y)) {
				ship[game.getLastMove().y][game.getLastMove().x].setIcon(fire);
				//Show what specific ship is hit
				nameShip.getKey(myHashMap, game.getLastMove());				
				ShipName = nameShip.getShipName();
				JOptionPane.showMessageDialog(null, "Hit " + ShipName);
				place_1 += 1;
			} 
		}
		//Place peg when miss
		if (ship[game.getLastMove().y][game.getLastMove().x].getIcon() == water) {
			ship[game.getLastMove().y][game.getLastMove().x].setIcon(dplante);
		}
		computer.setBackground(null);
		if (place_1 == 17) {
			JOptionPane.showMessageDialog(null, "The Computer wins");
		}
		model.switchturn();
		
	}
	
}