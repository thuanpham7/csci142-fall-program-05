package mvc;
import java.awt.Point;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

public class GetShipName {
	private String ShipName;
	public String getKey(HashMap<String, Vector<Point>>map, Point value){
		for(String o : map.keySet()) {
			for (int i = 0; i < map.get(o).size(); i++) {
				if (map.get(o).get(i).equals(value)) {
					ShipName = o;
				}
			}
		}
		return ShipName;
	}
	public String getShipName() { 
		return ShipName;
	}
}
