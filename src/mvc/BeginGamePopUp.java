package mvc;


import java.awt.Font;

import javax.swing.ImageIcon; 
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class BeginGamePopUp{

   public void popUpMessage() {
    	JLabel label = new JLabel("Welcome to BattleShip Game, Press OK to Continue");
    	label.setFont(new Font("Impact", Font.BOLD, 25));
    	ImageIcon image = new ImageIcon("battleshipBG2.jpg"); 
    	JOptionPane.showMessageDialog(null, label, "Begin BattleShip Game", JOptionPane.INFORMATION_MESSAGE, image);
    }
} 