package mvc;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;

import battleship.AILevel;
import battleship.GameModel;
import battleship.ShipBoard;
import battleship.DecisionMaker;
import javax.swing.ImageIcon;


public class CarsonEdits extends JFrame implements ActionListener {
	private static GameModel model;
	private static AILevel myMode;
	private static DecisionMaker game;
	private static ShipBoard placeShip;
	private static Point point;
	JButton[][] ship = new JButton[10][10];
	JButton[][] peg = new JButton[10][10];
	JButton player;
	JButton reset;
	JButton computer;
	JButton center;
	private static Vector<Point> myPoint;
	private static Vector<Point> compPoint;
	private static BeginGamePopUp pop;
	
	ImageIcon water = new ImageIcon("realwater.jpg");
	ImageIcon fire = new ImageIcon("giphy.gif");
	ImageIcon target = new ImageIcon("target.gif");
	ImageIcon bslogo = new ImageIcon("logo.jpg");
	ImageIcon dplante = new ImageIcon("dplanteresize.jpg");
	
	

	public static void main(String[] args) {
		pop = new BeginGamePopUp();
		pop.popUpMessage();
		String name = JOptionPane.showInputDialog("Enter player name");
		String[] options = {"CHEAT_MODE", "INTELLIGENT_MODE", "RANDOM_MODE"};
		int response = JOptionPane.showOptionDialog(null, "Select mode", "Welcome to BattleShipGame", 0, 1, null, options, options[0]);
		switch(response) {
		case 0: 
			model = new GameModel(name,AILevel.CHEAT_MODE);
			myMode = AILevel.CHEAT_MODE;
			model.beginGame();
			myPoint = model.getMyPoints();
			compPoint = model.getCompPoints();
			new CarsonEdits(name, null); 
			break;
		case 1: 
			placeShip = new ShipBoard();
			placeShip.placeShips();
			new CarsonEdits(name, null); 
			break;
		case 2: 
			model = new GameModel(name,AILevel.RANDOM_MODE);
			myMode = AILevel.RANDOM_MODE;
			game = new DecisionMaker(myMode, new ShipBoard());
			model.beginGame();
			myPoint = model.getMyPoints();
			compPoint = model.getCompPoints();
			new CarsonEdits(name, null); 
			break;
		}
	}

	public CarsonEdits(String name, ActionEvent e) {
		//Set up frame
		JFrame frame = new JFrame("BattleShip");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel ShipPanel = new JPanel();
		JPanel PegPanel = new JPanel();

		//Set up Ship Panel's button for placing ComputerPeg
		JPanel firstPanel = new JPanel();
		firstPanel.setLayout(new GridLayout(10, 10));
		for (int i=0; i< ship.length; i++) {
			for (int j=0; j<ship[i].length; j++) {
				ship[i][j] = new JButton();
				ship[i][j].setPreferredSize(new Dimension(60, 60));
				ship[i][j].addActionListener(this);
				firstPanel.add(ship[i][j]);
				ship[i][j].setOpaque(true);
				ship[i][j].setBorderPainted(false);
			}
		}

		//Set up PegPanel's Buttons for placing PlayerPeg
		JPanel secondPanel = new JPanel();
		secondPanel.setLayout(new GridLayout(10, 10));
		for (int i=0; i<peg.length; i++) {
			for (int j=0; j<peg[i].length; j++) {
				peg[i][j] = new JButton();
				peg[i][j].setPreferredSize(new Dimension(60, 60));
				peg[i][j].addActionListener(this);
				secondPanel.add(peg[i][j]);
				peg[i][j].setOpaque(true);
				peg[i][j].setBorderPainted(false);
			}
		}

		//Set up the player and the computer turn and the reset button

		ShipPanel.add(firstPanel);
		PegPanel.add(secondPanel);
		JPanel turn = new JPanel();
		//JPanel centerpic = new JPanel();
		
		turn.setLayout(new GridLayout(1,3));
		
		center = new JButton("", bslogo);
		player = new JButton(name + " Turn");
		reset = new JButton("Reset the game");
		computer = new JButton("Computer Turn");
		player.setPreferredSize(new Dimension(50,150));
		reset.setPreferredSize(new Dimension(50,50));
		computer.setPreferredSize(new Dimension(50,150));
		
		turn.add(player);
		turn.add(computer);
		//centerpic.add(center);
		
		player.addActionListener(this);
		computer.addActionListener(this);
		turn.add(reset);
		frame.add(ShipPanel, BorderLayout.WEST);
		frame.add(PegPanel, BorderLayout.EAST);
		frame.add(turn, BorderLayout.SOUTH);
		//frame.add(center, BorderLayout.CENTER);
		
		frame.setSize(520,600);
		frame.setMinimumSize(new Dimension(3000,3000));
		frame.setVisible(true);
		for (int i = 0; i< 10; i++) {
			for (int j = 0; j<10;j++) {
				peg[i][j].setIcon(water);
				peg[i][j].setOpaque(true);
				peg[i][j].setBorderPainted(false);
			}
		}
		for (int i = 0; i< 10; i++) {
			for (int j = 0; j<10;j++) {
				ship[i][j].setIcon(water);
				ship[i][j].setOpaque(true);
				ship[i][j].setBorderPainted(false);
				
			}
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (myMode == AILevel.CHEAT_MODE) {
			model.switchturn();
			player.setBackground(Color.BLUE);
			player.setOpaque(true);
			player.setBorderPainted(false);
			computer.setBackground(null);
			if (model.getComputerTurn() == false) {
				for (int i = 0; i< 10; i++) {
					for (int j = 0; j<10;j++) {
						for (int k = 0; k <compPoint.size(); k++) {
							if ((compPoint.get(k).x == j) && (compPoint.get(k).y == i)){
								if (e.getSource()==peg[i][j]) {
									peg[compPoint.get(k).y][compPoint.get(k).x].setIcon(fire);
//									peg[compPoint.get(k).y][compPoint.get(k).x].setOpaque(true);
//									peg[compPoint.get(k).y][compPoint.get(k).x].setBorderPainted(false);
								} 
							}
						}
						if (e.getSource()==peg[i][j]) {
							if (peg[i][j].getIcon() == water) {
								peg[i][j].setIcon(dplante);
//								peg[i][j].setOpaque(true);
//								peg[i][j].setBorderPainted(false);
								
							}
						}
					}
				}
			}
			if (e.getSource() == computer){
				computer.setBackground(Color.BLUE);
				computer.setOpaque(true);
				computer.setBorderPainted(false);
				
				if (player.isBackgroundSet() == true) {
					player.setBackground(Color.WHITE);
					player.setOpaque(true);
					player.setBorderPainted(false);
				}
				for (int i = 0; i< 10; i++) {
					for (int j = 0; j<10;j++) {
						for (int k = 0; k <myPoint.size(); k++) {
							if ((myPoint.get(k).x == j) && (myPoint.get(k).y == i)){
								ship[i][j].setIcon(fire);
								ship[i][j].setOpaque(true);
								ship[i][j].setBorderPainted(false);
							}
						}
					}
				}
			}
		}

		else if (myMode == AILevel.RANDOM_MODE) {
			model.switchturn();
			player.setBackground(Color.BLUE);
			player.setOpaque(true);
			player.setBorderPainted(false);
			computer.setBackground(null);
			if (model.getComputerTurn() == false) {
				for (int i = 0; i< 10; i++) {
					for (int j = 0; j<10;j++) {
						for (int k = 0; k <compPoint.size(); k++) {
							if ((compPoint.get(k).x == j) && (compPoint.get(k).y == i)){
								if (e.getSource()==peg[i][j]) {
									peg[compPoint.get(k).y][compPoint.get(k).x].setIcon(fire);
									peg[compPoint.get(k).y][compPoint.get(k).x].setOpaque(true);
									peg[compPoint.get(k).y][compPoint.get(k).x].setBorderPainted(false);
									
								} 
							}
						}
						if (e.getSource()==peg[i][j]) {
							if (peg[i][j].getIcon() == water) {
								peg[i][j].setIcon(dplante);
//								peg[i][j].setOpaque(true);
//								peg[i][j].setBorderPainted(false);
								
							}
						}
					}
				}
			}
			if (e.getSource() == computer){
				computer.setBackground(Color.BLUE);
				computer.setOpaque(true);
				computer.setBorderPainted(false);
				
				if (player.isBackgroundSet() == true) {
					
					player.setBackground(Color.WHITE);
				}
				game.nextMove();
				
				for (int i = 0; i < myPoint.size(); i++) {
					if ((game.getMove().x == myPoint.get(i).x) && (game.getMove().y == myPoint.get(i).y)) {
						ship[game.getMove().y][game.getMove().x].setIcon(fire);
//						ship[game.getMove().y][game.getMove().x].setOpaque(true);
//						ship[game.getMove().y][game.getMove().x].setBorderPainted(false);
					} 
					}
				if (ship[game.getMove().y][game.getMove().x].getIcon() == water) {
					ship[game.getMove().y][game.getMove().x].setIcon(dplante);
					
//					ship[game.getMove().y][game.getMove().x].setOpaque(true);
//					ship[game.getMove().y][game.getMove().x].setBorderPainted(false);
				}
			}
		}	
	}
}
