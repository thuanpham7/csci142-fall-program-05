package mvc;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PictureButtons extends JFrame{
	
	JButton one, two, three;
	
	public static void main(String[] args) {
		
		new PictureButtons();
	}
	
	public PictureButtons()
	{
		
		ImageIcon water = new ImageIcon("ocean.gif");
		ImageIcon fire = new ImageIcon("giphy.gif");
		ImageIcon target = new ImageIcon("target.gif");
		
		one = new JButton("", water);
		two = new JButton("", fire);
		three = new JButton("", target);
		
		one.setPreferredSize(new Dimension(125,125));
		two.setPreferredSize(new Dimension(125,125));
		three.setPreferredSize(new Dimension(125,125));
		
		setLayout(new FlowLayout());
		add(one);
		add(two);
		add(three);
		
		setVisible(true);
		setSize(500, 500);
		setLocation(10, 10);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}