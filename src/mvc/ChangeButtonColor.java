package mvc;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ChangeButtonColor implements ActionListener{
	
	JButton hit, black, missed;
	
	public static void main(String[] args) {
		//ChangeButtonColor c = new ChangeButtonColor();
		
		new ChangeButtonColor(null);
	}
	
	public ChangeButtonColor(ActionEvent e) {
		hit = new JButton();
		black = new JButton();
		missed = new JButton();
		
		JFrame frame = new JFrame();
		JPanel panel = new JPanel();
		
		frame.setSize(10, 10);
		hit.setPreferredSize(new Dimension(100, 100));
		black.setPreferredSize(new Dimension(100, 100));
		missed.setPreferredSize(new Dimension(100, 100));
		
		panel.add(hit);
		panel.add(black);
		panel.add(missed);
		
		frame.add(panel);
		frame.setMinimumSize(new Dimension(500, 500));
		frame.setVisible(true);
		
		hit.addActionListener(this);
		black.addActionListener(this);
		missed.addActionListener(this);
		
	}
		@Override
		public void actionPerformed(ActionEvent e) {
			String click = e.getActionCommand();
			
			if(e.getSource() == hit) {
				System.out.println(click + "ship hit!");
			}
			
			if(e.getSource() == black) {
				System.out.println("ship position");
			}
			
			if(e.getSource() == missed) {
				System.out.println("missed");
			}
			
			if(e.getSource() == hit) {
				hit.setBackground(Color.red);
				hit.setOpaque(true);
				hit.setBorderPainted(false);
			}
			else if(e.getSource() == black) {
				black.setBackground(Color.black);
				black.setOpaque(true);
				black.setBorderPainted(false);
			}
			else if(e.getSource() == missed) {
				missed.setBackground(Color.white);
				missed.setOpaque(true);
				missed.setBorderPainted(false);
			}
		}
	}	